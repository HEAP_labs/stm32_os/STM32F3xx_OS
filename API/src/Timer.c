/**
  ******************************************************************************
  * @file    ./API/src/Timer.c 
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   API f�r Timer
  */

#include "PerUsings.h"
#ifdef __USING_TIMER

/* Includes ------------------------------------------------------------------*/
#include "Timer.h"

/* Private typedef -----------------------------------------------------------*/
TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
NVIC_InitTypeDef         NVIC_InitStructure;

/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
API_TIM_type_t API_TIM1    = { TIM1,  RCC_APB2Periph_TIM1,  TIM1_CC_IRQn,            };
API_TIM_type_t API_TIM2    = { TIM2,  RCC_APB1Periph_TIM2,  TIM2_IRQn,               };
API_TIM_type_t API_TIM3    = { TIM3,  RCC_APB1Periph_TIM3,  TIM3_IRQn,               };
API_TIM_type_t API_TIM6    = { TIM6,  RCC_APB1Periph_TIM6,  TIM6_DAC_IRQn,           };
API_TIM_type_t API_TIM7    = { TIM7,  RCC_APB1Periph_TIM7,  TIM7_IRQn,               };
API_TIM_type_t API_TIM15   = { TIM15, RCC_APB2Periph_TIM15, TIM1_BRK_TIM15_IRQn,     };
API_TIM_type_t API_TIM16   = { TIM16, RCC_APB2Periph_TIM16, TIM1_UP_TIM16_IRQn,      };
API_TIM_type_t API_TIM17   = { TIM17, RCC_APB2Periph_TIM17, TIM1_TRG_COM_TIM17_IRQn, };

/* Private function prototypes -----------------------------------------------*/
void API_start_taskclass(API_TIM_type_t* API_TIMx, double t_period_s, uint8_t priority);
void API_PWM_start(API_TIM_type_t* API_TIMx, uint32_t CHx, API_GPIO_type_t* portpin, uint8_t GPIO_AF_x, double t_period_s, double duty_cycle, uint16_t TIM_OCPolarity);
static void timer_clock_enable(API_TIM_type_t* API_TIMx);
static uint32_t timer_get_clock_frequency(API_TIM_type_t* API_TIMx);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Start a new Taskclass
  * @param  API_TIMx:   selected API_TIMx
  * @param  t_period_s: Time for one period
  * @param  priority:   interrupt priority (0..15)
  * @retval none
  * @note   bug: doesn't work with TIM1
  */
void API_start_taskclass(API_TIM_type_t* API_TIMx, double t_period_s, uint8_t priority)
{
  uint32_t period_timer_ticks = 0;
  uint32_t prescaler = 0;
  uint32_t timer_clock = 0;

  /* TIMx clock enable */
  timer_clock_enable(API_TIMx);
  
  timer_clock = timer_get_clock_frequency(API_TIMx);

  /* Prescaler berechnen */
  if(API_TIMx->TIMx == API_TIM2.TIMx){
    prescaler = (uint16_t)((timer_clock) * t_period_s / 0x100000000);
  }
  else{
    prescaler = (uint16_t)((timer_clock) * t_period_s / 0x10000);
  }

  period_timer_ticks = (timer_clock * t_period_s / ((prescaler + 1))); 
  
  /* Time Base configuration */
  TIM_TimeBaseStructure.TIM_Prescaler = prescaler;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = period_timer_ticks;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;

  TIM_TimeBaseInit(API_TIMx->TIMx, &TIM_TimeBaseStructure);

  /* TIMx counter enable */
  TIM_Cmd(API_TIMx->TIMx, ENABLE);
  
  /* Interrupt konfigurieren */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

  NVIC_InitStructure.NVIC_IRQChannel = API_TIMx->IRQnx;               // Interrupt Source ausw�hlen
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = priority; // pre-emption Priorit�t ausw�hlen
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;               // sub priority ausw�hlen
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;                  // enablen
  NVIC_Init(&NVIC_InitStructure);                                  // Parameter �bernehmen

  TIM_ITConfig(API_TIMx->TIMx, TIM_IT_Update, ENABLE);
}

/**
  * @brief  Start a new PWM Signal on a GPIO
  * @param  API_TIMx:       selected API_TIMx
  * @param  CHx:            Timer-channel (can be CH1, CH2, CH3 or CH4)
  * @param  portpin:        Output Pin for the PWM, can be PA0..PA15, ..., PI0..PI15 (depends on Timer and Timer-channel)
  * @param  GPIO_AF_x:      Alternate Function for selected portpin
  * @param  t_period_s:     Time for one period
  * @param  duty_cycle:     PWM duty cycle [0..1]
  * @param  TIM_OCPolarity: can be TIM_OCPolarity_High or TIM_OCPolarity_Low
  * @retval none
  */
void API_PWM_start(API_TIM_type_t* API_TIMx, uint32_t CHx, API_GPIO_type_t* portpin, uint8_t GPIO_AF_x, double t_period_s, double duty_cycle, uint16_t TIM_OCPolarity){
  TIM_OCInitTypeDef TIM_OCInitStructure;
  GPIO_InitTypeDef  GPIO_InitStructure;

  uint32_t pulse_timer_ticks  = 0;
  uint32_t period_timer_ticks = 0;
  uint16_t prescaler         = 0;
  uint16_t pin_source         = 0;
  uint32_t timer_clock        = 0;

  while((portpin->GPIO_Pin_x >> pin_source) > 0x01) pin_source++;

  /* TIMx clock enable */
  timer_clock_enable(API_TIMx);
  
  timer_clock = timer_get_clock_frequency(API_TIMx);

  /* Prescaler berechnen */
  if(API_TIMx->TIMx == API_TIM2.TIMx){
    prescaler = (uint16_t)((timer_clock) * t_period_s / 0x100000000);
  }
  else{
    prescaler = (uint16_t)((timer_clock) * t_period_s / 0x10000);
  }

  period_timer_ticks = (timer_clock * t_period_s / ((prescaler + 1))); 
  pulse_timer_ticks  = period_timer_ticks * duty_cycle;

  /* GPIOx Periph clock enable */
  RCC_AHBPeriphClockCmd(portpin->RCC_AHBPeriph_GPIOx, ENABLE);

  /* Set AF Pin */
  GPIO_PinAFConfig(portpin->GPIOx, pin_source, GPIO_AF_x);

  GPIO_InitStructure.GPIO_Pin = portpin->GPIO_Pin_x;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(portpin->GPIOx, &GPIO_InitStructure);

  /* Time Base configuration */
  TIM_TimeBaseStructure.TIM_Prescaler = prescaler;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = period_timer_ticks;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;

  TIM_TimeBaseInit(API_TIMx->TIMx, &TIM_TimeBaseStructure);

  /* TIMx counter enable */
  TIM_Cmd(API_TIMx->TIMx, ENABLE);


  /* PWM Mode configuration */
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = pulse_timer_ticks;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity;

  if(CHx == CH1)      { TIM_OC1Init(API_TIMx->TIMx, &TIM_OCInitStructure); TIM_OC1PreloadConfig(API_TIMx->TIMx, TIM_OCPreload_Enable); }
  else if(CHx == CH2) { TIM_OC2Init(API_TIMx->TIMx, &TIM_OCInitStructure); TIM_OC2PreloadConfig(API_TIMx->TIMx, TIM_OCPreload_Enable); }
  else if(CHx == CH3) { TIM_OC3Init(API_TIMx->TIMx, &TIM_OCInitStructure); TIM_OC3PreloadConfig(API_TIMx->TIMx, TIM_OCPreload_Enable); }
  else if(CHx == CH4) { TIM_OC4Init(API_TIMx->TIMx, &TIM_OCInitStructure); TIM_OC4PreloadConfig(API_TIMx->TIMx, TIM_OCPreload_Enable); }
  //else;
}

/**
  * @brief  enable clock of TIMx
  * @param  API_TIMx: selected API_TIMx
  * @retval none
  */
void timer_clock_enable(API_TIM_type_t* API_TIMx)
{
  if(API_TIMx->TIMx == API_TIM1.TIMx)       RCC_APB2PeriphClockCmd(API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else if(API_TIMx->TIMx == API_TIM15.TIMx) RCC_APB2PeriphClockCmd(API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else if(API_TIMx->TIMx == API_TIM16.TIMx) RCC_APB2PeriphClockCmd(API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else if(API_TIMx->TIMx == API_TIM17.TIMx) RCC_APB2PeriphClockCmd(API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
  else                                      RCC_APB1PeriphClockCmd(API_TIMx->RCC_APBxPeriph_TIMx, ENABLE);
}

/**
  * @brief  read clock frequencie of TIMx
  * @param  API_TIMx: selected API_TIMx
  * @retval clock frequency
  */
uint32_t timer_get_clock_frequency(API_TIM_type_t* API_TIMx)
{
  RCC_ClocksTypeDef RCC_Clocks;
  
  RCC_GetClocksFreq(&RCC_Clocks);
  
  if(API_TIMx->TIMx == API_TIM1.TIMx)       return RCC_Clocks.TIM1CLK_Frequency;
  else if(API_TIMx->TIMx == API_TIM2.TIMx)  return RCC_Clocks.PCLK1_Frequency * 2; // don't use .TIM3CLK_Frequency because it's not calculated for STM32F334 (SPL V1.2.3)
  else if(API_TIMx->TIMx == API_TIM3.TIMx)  return RCC_Clocks.PCLK1_Frequency * 2; // don't use .TIM4CLK_Frequency because it's not calculated for STM32F334 (SPL V1.2.3)
  else if(API_TIMx->TIMx == API_TIM6.TIMx)  return RCC_Clocks.PCLK1_Frequency * 2; // .TIM6CLK_Frequency not defined in RCC_ClocksTypeDef (SPL V1.2.3)
  else if(API_TIMx->TIMx == API_TIM7.TIMx)  return RCC_Clocks.PCLK1_Frequency * 2; // .TIM7CLK_Frequency not defined in RCC_ClocksTypeDef (SPL V1.2.3)
  else if(API_TIMx->TIMx == API_TIM15.TIMx) return RCC_Clocks.TIM15CLK_Frequency;
  else if(API_TIMx->TIMx == API_TIM16.TIMx) return RCC_Clocks.TIM16CLK_Frequency;
  else if(API_TIMx->TIMx == API_TIM17.TIMx) return RCC_Clocks.PCLK2_Frequency; // don't use .TIM17CLK_Frequency because there is a bug in SPL V1.2.3 (writes to TIM16CLK_Frequency)
  else                                      return 0;
}

#endif // __USING_TIMER
