/**
  ******************************************************************************
  * @file    ./API/src/USART.c 
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   API f�r USART
  */

#include "PerUsings.h"
#ifdef __USING_USART

/* Includes ------------------------------------------------------------------*/
#include "USART.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
API_USART_type_t API_USART1 = { USART1, RCC_APB2Periph_USART1, USART1_IRQn };
API_USART_type_t API_USART2 = { USART2, RCC_APB1Periph_USART2, USART2_IRQn };
API_USART_type_t API_USART3 = { USART3, RCC_APB1Periph_USART3, USART3_IRQn };

/* Private function prototypes -----------------------------------------------*/
void API_USART_init(API_USART_type_t* API_USARTx, API_GPIO_type_t* tx_portpin, API_GPIO_type_t* rx_portpin, uint32_t baudrate, uint8_t priority);
void API_USART_send_byte(API_USART_type_t* API_USARTx, uint8_t data);
void API_USART_send_string(API_USART_type_t* API_USARTx, const char* format_string, ...);
void API_USART_send_data(API_USART_type_t* API_USARTx, uint8_t *p_tx_data, uint32_t tx_length);
uint8_t API_USART_get_byte_from_buffer(API_USART_type_t* API_USARTx);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  init USARTx
  * @param  API_USARTx: can be API_USART1..API_USART3
  * @param  tx_portpin: Tx pin for USARTx, can be PA0..PA15, ..., PH0..PH15 (depends on selected USART)
  * @param  rx_portpin: Rx pin for USARTx, can be PA0..PA15, ..., PH0..PH15 (depends on selected USART)
  * @param  baudrate:   Baudrate for the selected USART
  * @param  priority:   interrupt priority of received handler
  * @retval none
  */
void API_USART_init(API_USART_type_t* API_USARTx, API_GPIO_type_t* tx_portpin, API_GPIO_type_t* rx_portpin, uint32_t baudrate, uint8_t priority)
{
  USART_InitTypeDef  USART_InitStructure;
  GPIO_InitTypeDef   GPIO_InitStructure;
  NVIC_InitTypeDef   NVIC_InitStructure;
  
  uint16_t PinSource_tx = 0;
  uint16_t PinSource_rx = 0;

  while((tx_portpin->GPIO_Pin_x >> PinSource_tx) > 0x01) PinSource_tx++;
  while((rx_portpin->GPIO_Pin_x >> PinSource_rx) > 0x01) PinSource_rx++;

  /* USART_x Periph clock enable */
  if(API_USARTx->USARTx == API_USART1.USARTx) RCC_APB2PeriphClockCmd(API_USARTx->RCC_APBxPeriph_USARTx, ENABLE);
  else                                        RCC_APB1PeriphClockCmd(API_USARTx->RCC_APBxPeriph_USARTx, ENABLE);

  /* GPIOx Periph clock enable */
  RCC_AHBPeriphClockCmd(tx_portpin->RCC_AHBPeriph_GPIOx, ENABLE);
  RCC_AHBPeriphClockCmd(rx_portpin->RCC_AHBPeriph_GPIOx, ENABLE);

  /* Set AF Pin */
  GPIO_PinAFConfig(tx_portpin->GPIOx, PinSource_tx, GPIO_AF_7);
  GPIO_PinAFConfig(rx_portpin->GPIOx, PinSource_rx, GPIO_AF_7);

  GPIO_InitStructure.GPIO_Pin = tx_portpin->GPIO_Pin_x;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(tx_portpin->GPIOx, &GPIO_InitStructure);
  
  GPIO_InitStructure.GPIO_Pin = rx_portpin->GPIO_Pin_x;
  GPIO_Init(rx_portpin->GPIOx, &GPIO_InitStructure);

  /* configure USART */
  USART_InitStructure.USART_BaudRate = baudrate;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_Init(API_USARTx->USARTx, &USART_InitStructure);
  
  /* USART enable */
  USART_Cmd(API_USARTx->USARTx, ENABLE);
  
  /* configure NVIC */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

  NVIC_InitStructure.NVIC_IRQChannel = API_USARTx->IRQnx;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = priority;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  /* Interrupt enable */
  USART_ITConfig(API_USARTx->USARTx, USART_IT_RXNE, ENABLE);
}

/**
  * @brief  Send a single Byte using USART_x
  * @param  API_USARTx: can be API_USART1..API_USART3
  * @param  data:       Data to send
  * @retval none
  */
void API_USART_send_byte(API_USART_type_t* API_USARTx, uint8_t data)
{  
  while(USART_GetFlagStatus(API_USARTx->USARTx, USART_FLAG_TXE) == RESET);
  USART_SendData(API_USARTx->USARTx, data);
}

/**
  * @brief  Send a String using USART_x
  * @param  API_USARTx: can be API_USART1..API_USART3
  * @param  All other parameters like printf()
  * @note   max string length: 100 byte
  * @retval none
  */
void API_USART_send_string(API_USART_type_t* API_USARTx, const char* format_string, ...)
{
  va_list  args;
  int32_t  n_size = 0;
  char     send_string[100];

  /* format string */
  va_start(args, format_string);
  n_size = vsnprintf(send_string, 100, format_string, args);
  va_end(args);

  /* send string */
  API_USART_send_data(API_USARTx, (uint8_t*)send_string, n_size);
}

/**
  * @brief  Send a single Byte using USARTx
  * @param  API_USARTx: can be API_USART1..API_USART3
  * @param  *p_tx_data: Pointer to Data to send
  * @param  tx_length:  Number of bytes to send
  * @retval none
  */
void API_USART_send_data(API_USART_type_t* API_USARTx, uint8_t *p_tx_data, uint32_t tx_length)
{
  uint32_t i;
  
  for(i = 0; i < tx_length; i++)
  {
    API_USART_send_byte(API_USARTx, p_tx_data[i]);
  }
}

/**
  * @brief  Get a single Byte from USART_x buffer
  * @param  API_USARTx: can be API_USART1..API_USART3
  * @retval ReceivedByte: most recent received byte by the USARTx peripheral
  */
uint8_t API_USART_get_byte_from_buffer(API_USART_type_t* API_USARTx)
{
  uint8_t received_byte = 0;

  received_byte = (uint8_t)USART_ReceiveData(API_USARTx->USARTx);

  return received_byte;
}

#endif // __USING_USART
