/**
  ******************************************************************************
  * @file    ./API/inc/Exti.h
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   API f�r External Interrupts
  */
	
	
#ifndef __EXTI_H
#define __EXTI_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "GPIO.h"


/* Exported types ------------------------------------------------------------*/


/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void API_conf_EXTI(API_GPIO_type_t* portpin, uint32_t EXTI_Line, uint8_t NVIC_IRQChannel, uint8_t priority);

#endif
