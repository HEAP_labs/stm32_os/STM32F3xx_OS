/**
  ******************************************************************************
  * @file    ./API/inc/Timer.h
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   API f�r Timer
  */
  
#ifndef __TIMER_H
#define __TIMER_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "GPIO.h"

/* Exported types ------------------------------------------------------------*/
typedef void (*IRQ_Handler)(void); 

typedef struct{
  TIM_TypeDef*  TIMx;
  __IO uint32_t RCC_APBxPeriph_TIMx;
  IRQn_Type     IRQnx;
} API_TIM_type_t;

/* Exported constants --------------------------------------------------------*/
#define CH1 0x01
#define CH2 0x02
#define CH3 0x04
#define CH4 0x08

extern API_TIM_type_t API_TIM1;
extern API_TIM_type_t API_TIM2;
extern API_TIM_type_t API_TIM3;
extern API_TIM_type_t API_TIM6;
extern API_TIM_type_t API_TIM7;
extern API_TIM_type_t API_TIM15;
extern API_TIM_type_t API_TIM16;
extern API_TIM_type_t API_TIM17;

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void API_start_taskclass(API_TIM_type_t* API_TIMx, double t_period_s, uint8_t priority);
extern void API_PWM_start(API_TIM_type_t* API_TIMx, uint32_t CHx, API_GPIO_type_t* portpin, uint8_t GPIO_AF_x, double t_period_s, double duty_cycle, uint16_t TIM_OCPolarity);

#endif
