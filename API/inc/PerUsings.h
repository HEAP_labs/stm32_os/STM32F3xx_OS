/**
  ******************************************************************************
  * @file    ./API/inc/PerUsings.h 
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   peripherial usage definitions
  */

#ifndef __PERUSINGS_H
#define __PERUSINGS_H

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define __USING_GPIO
//#define __USING_EXTI
#define __USING_TIMER
//#define __USING_USART
//#define __USING_SPI

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif // __PERUSINGS_H
