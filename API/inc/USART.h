/**
  ******************************************************************************
  * @file    ./API/inc/USART.h
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   API f�r USART
  */

#ifndef __USART_H
#define __USART_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "GPIO.h"
#include "stdarg.h"
#include "stdio.h"

/* Exported types ------------------------------------------------------------*/
typedef struct{
  USART_TypeDef* USARTx;
  __IO uint32_t  RCC_APBxPeriph_USARTx;
  IRQn_Type      IRQnx;
} API_USART_type_t;

/* Exported constants --------------------------------------------------------*/
extern API_USART_type_t API_USART1;
extern API_USART_type_t API_USART2;
extern API_USART_type_t API_USART3;

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void API_USART_init(API_USART_type_t* API_USARTx, API_GPIO_type_t* tx_portpin, API_GPIO_type_t* rx_portpin, uint32_t baudrate, uint8_t priority);
extern void API_USART_send_byte(API_USART_type_t* API_USARTx, uint8_t data);
extern void API_USART_send_string(API_USART_type_t* API_USARTx, const char* format_string, ...);
extern void API_USART_send_data(API_USART_type_t* API_USARTx, uint8_t *p_tx_data, uint32_t tx_length);
extern uint8_t API_USART_get_byte_from_buffer(API_USART_type_t* API_USARTx);

#endif
