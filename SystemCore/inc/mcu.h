/**
  ******************************************************************************
  * @file    ./System_Core/inc/mcu.h 
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   MCU definition
  */

#ifndef __MCU_H
#define __MCU_H

/* Exported constants --------------------------------------------------------*/
#ifndef STM32F334x8
    #define STM32F334x8
#endif // !STM32F334x8

/* Includes ------------------------------------------------------------------*/
#include "stm32f30x.h"

/* Exported types ------------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif
