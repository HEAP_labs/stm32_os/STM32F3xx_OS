/**
  ******************************************************************************
  * @file    stm32f30x_it.c 
  * @author  MCD Application Team
  * @version V1.2.2
  * @date    14-August-2015
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f30x_it.h"

/** @addtogroup STM32F30x_StdPeriph_Templates
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
extern void taskclass(void);

/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
}

/******************************************************************************/
/*                 STM32F30x Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f30x.s).                                            */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

#ifdef __USING_TIMER

void TIM1_CC_IRQHandler(void)
{
  if (TIM_GetITStatus(TIM1, TIM_IT_Update) != RESET)
  {

    TIM_ClearITPendingBit(TIM1, TIM_IT_Update);
  }
}

void TIM2_IRQHandler(void)
{
  if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
  {

    TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
  }
}

void TIM3_IRQHandler(void)
{
  if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
  {
    taskclass();
    TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
  }
}

void TIM6_DAC_IRQHandler(void)
{
  if (TIM_GetITStatus(TIM6, TIM_IT_Update) != RESET)
  {

    TIM_ClearITPendingBit(TIM6, TIM_IT_Update);
  }
}

void TIM7_IRQHandler(void)
{
  if (TIM_GetITStatus(TIM7, TIM_IT_Update) != RESET)
  {

    TIM_ClearITPendingBit(TIM7, TIM_IT_Update);
  }
}

void TIM1_BRK_TIM15_IRQHandler(void)
{
  if (TIM_GetITStatus(TIM15, TIM_IT_Update) != RESET)
  {

    TIM_ClearITPendingBit(TIM15, TIM_IT_Update);
  }
}

void TIM1_UP_TIM16_IRQHandler(void)
{
  if (TIM_GetITStatus(TIM16, TIM_IT_Update) != RESET)
  {

    TIM_ClearITPendingBit(TIM16, TIM_IT_Update);
  }
}

void TIM1_TRG_COM_TIM17_IRQHandler(void)
{
  if (TIM_GetITStatus(TIM17, TIM_IT_Update) != RESET)
  {

    TIM_ClearITPendingBit(TIM17, TIM_IT_Update);
  }
}

#endif // __USING_TIMER

#ifdef __USING_USART

void USART1_IRQHandler(void)
{
  if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
  {

    USART_ClearITPendingBit(USART1, USART_IT_RXNE);
  }
}

void USART2_IRQHandler(void)
{
  if (USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
  {

    USART_ClearITPendingBit(USART2, USART_IT_RXNE);
  }
}

void USART3_IRQHandler(void)
{
  if (USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
  {
    USART_ClearITPendingBit(USART3, USART_IT_RXNE);
  }
}

#endif // __USING_USART

#ifdef __USING_SPI

void DMA1_Channel3_IRQHandler(void)
{
  if (DMA_GetITStatus(DMA1_IT_TC3) != RESET)
  {
    DMA_ClearITPendingBit(DMA1_IT_TC3);
  }
}

void DMA1_Channel2_IRQHandler(void)
{
  if (DMA_GetITStatus(DMA1_IT_TC2) != RESET)
  {
    DMA_ClearITPendingBit(DMA1_IT_TC2);
    
  }
}

void SPI1_IRQHandler(void)
{
  if (SPI_I2S_GetITStatus(SPI1, SPI_I2S_IT_RXNE) != RESET)
  {
    SPI_I2S_ITConfig(SPI1, SPI_I2S_IT_RXNE, DISABLE);
  }
}

#endif // __USING_SPI


#ifdef __USING_EXTI

void EXTI0_IRQHandler(void){
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line0) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line0);
    }
}

void EXTI1_IRQHandler(void){
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line1) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line1);
    }
}

void EXTI2_TSC_IRQHandler(void){
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line2) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line2);
    }
}

void EXTI3_IRQHandler(void){
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line3) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line3);
    }
}

void EXTI4_IRQHandler(void){
	/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line4) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line4);
    }	
}

void EXTI9_5_IRQHandler(void){
	/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line5) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line5);
    }
		
	/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line6) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line6);
    }
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line7) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line7);
    }
		
    if (EXTI_GetITStatus(EXTI_Line8) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line8);
    }
		
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line9) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line9);
    }
}

void EXTI15_10_IRQHandler(void){
	
	 /* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line10) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line10);
    }
		
	  /* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line11) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line11);
    }
		
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line12) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line12);
    }
		
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line13) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line13);
    }
		
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line14) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line14);
    }
		
		/* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line15) != RESET) {
        /* Do your stuff here */
        
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line15);
    }
		
}
#endif // __USING_EXTI

/**
  * @}
  */ 


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
