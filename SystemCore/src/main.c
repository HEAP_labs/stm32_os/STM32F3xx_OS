/**
  ******************************************************************************
  * @file    ./System_Core/src/main.c 
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   main module
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
extern void startup_sequence(void);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  */
int main(void){
  startup_sequence();
  
  while(1)
  {
    
  }
}
