#ifndef __LIBRARY_H
#define __LIBRARY_H

#include "mcu.h"
#include "LibUsings.h"

#ifdef __USING_ADXL345
  #include "ADXL345.h"
#endif // __USING_ADXL345

#endif // __LIBRARY_H
