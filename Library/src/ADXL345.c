/**
  ******************************************************************************
  * @file    ./Library/src/ADXL345.c 
  * @author  Andreas Hirtenlehner
  * @brief   Library for accelerometer ADXL345
  */
  
#include "LibUsings.h"
#ifdef __USING_ADXL345

/* Includes ------------------------------------------------------------------*/
#include "ADXL345.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
uint8_t* ADXL345_get_pon_commands(void); 

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  power on commands
  * @note   set 2 registers (4 bytes to send)
  * @note   register and data bytes alternating
  */
uint8_t* ADXL345_get_pon_commands(void)
{
  static uint8_t pon_commands[] = { ADXL345_BW_RATE,   0x0F,   // Set Update Rate (0x0A=100Hz)
                                    ADXL345_OFSX,      0x00,   // reset offset register
                                    ADXL345_POWER_CTL, 0x18 }; // enable Auto-Sleep and measure mode
  
  return pon_commands;
}


#endif // __USING_ADXL345
