/**
  ******************************************************************************
  * @file    ./Application/inc/application.h
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   Header file for the application module
  */

#ifndef __APPLICATION_H
#define __APPLICATION_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

/*!
  * @brief Baudrate definitions
*/
#define USART1_BAUDRATE  ((uint32_t)38400)
#define USART2_BAUDRATE  ((uint32_t)115200)

#define SPI1_BAUDRATE_PRESCALER  SPI_BaudRatePrescaler_64


/*!
  * @brief Portpin definitions
*/
#define USART1_RX ((API_GPIO_type_t *)&PA10)
#define USART1_TX ((API_GPIO_type_t *)&PA9)

#define USART2_RX ((API_GPIO_type_t *)&PA3)
#define USART2_TX ((API_GPIO_type_t *)&PA2)

#define SPI1_MOSI ((API_GPIO_type_t *)&PB5)
#define SPI1_MISO ((API_GPIO_type_t *)&PB4)
#define SPI1_SCK  ((API_GPIO_type_t *)&PB3)
#define SPI1_NSS  ((API_GPIO_type_t *)&PA15)

/*!
  * @brief NUCLEO definitions
*/
#define LD2 ((API_GPIO_type_t *)&PA5)


/*!
  * @brief measurement_state_machine period time [s]
*/
#define T_PERIOD_S ((double)1e-3)

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif /* __APPLICATION_H */

