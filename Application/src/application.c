/**
  ******************************************************************************
  * @file    ./Application/src/application.c 
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   Implementation of the application code
  */
  
/* Includes ------------------------------------------------------------------*/
#include "application.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void taskclass(void);
void startup_sequence(void);
static void clock_config(void);

/* Private functions ---------------------------------------------------------*/
void taskclass(void)
{
  static uint8_t iniOK = 0;


  /*** BLINK ***/
  /*{
    static double t = 0;
    static double t_blink_s = 1.0;
  
    if(!iniOK)                          { t = 0;                          }
    else if(t + T_PERIOD_S < t_blink_s) { t = t + T_PERIOD_S;             }
    else                                { t = t + T_PERIOD_S - t_blink_s; }
  
    if(!iniOK)                   { SetDO(LD2, Bit_SET);   }
    else if(t < t_blink_s * 0.5) { SetDO(LD2, Bit_SET);   }
    else                         { SetDO(LD2, Bit_RESET); }
  }*/
  
#ifdef __USING_TIMER
  /*** PWM example ***/
  {
    if(!iniOK) API_PWM_start(&API_TIM2, CH1, LD2, GPIO_AF_1, 1.0, 0.2, TIM_OCPolarity_High);
    //else;
  }
#endif // __USING_TIMER
  
  
#ifdef __USING_SPI
  /*** SPI example ***/
  {
    static uint8_t tx_data[] = { 0x22, 0x42, 0x42, 0x75, 0x45 };
    static uint8_t rx_data[2];
            
    if(!iniOK) 
    {
      API_SPI_init(&API_SPI1, SPI1_MOSI, SPI1_MISO, SPI1_SCK, SPI1_NSS, SPI1_BAUDRATE_PRESCALER, SPI_MODE_1); 
      API_SPI_DMA_init(&API_SPI1, tx_data, rx_data);
    }
    else
    {
      /* send Byte */
      {
        //API_SPI_send_byte(&API_SPI1, 0x22);
      }
      
      /* send data using DMA */
      {
        //API_SPI_DMA_send(&API_SPI1, 5);
      }
      
      /* send and receive data simultaneously using DMA */
      {
        //API_SPI_DMA_send_and_receive(&API_SPI1, 2);
      }
      
      /* send and receive data successively using DMA */
      /*{
        uint8_t errno = API_SPI_DMA_send_then_receive_statemachine(&API_SPI1, T_PERIOD_S, 5, 2);
        
        if(errno == SPI_OK)
        {
          //tramission complete
        }
        else if(errno == SPI_BUSY)
        {
          //transmitting
        }
        else if(errno == SPI_ERROR)
        {
          //SPI error
        }
        //else;
      }*/
    }
  }
#endif // __USING_SPI
  
#ifdef __USING_USART
  /*** USART example ***/
  {
    if(!iniOK) API_USART_init(&API_USART2, USART2_TX, USART2_RX, USART2_BAUDRATE, 2);
    else
    {
      //API_USART_send_byte(&API_USART1, 0x35);
      
      //API_USART_send_string(&API_USART2, "iniOK=%i\n", iniOK);
    }
  }
#endif // __USING_USART
	

#ifdef __USING_EXTI
	{
//		if(!iniOK){
//			API_conf_EXTI(&PC13, EXTI_Line13, EXTI15_10_IRQn, 0);
//			API_conf_EXTI(&PA0, EXTI_Line0, EXTI0_IRQn, 0);
//			API_conf_EXTI(&PA1, EXTI_Line1, EXTI1_IRQn, 0);
//			API_conf_EXTI(&PC2, EXTI_Line2, EXTI2_TS_IRQn, 0);
//			API_conf_EXTI(&PC3, EXTI_Line3, EXTI3_IRQn, 0);
//		}
	}
#endif // __USING_EXTI

  iniOK = 1;
}

/*
* @brief startup - function, called in the main module
* @param[in] NONE
* @return NONE
*/
void startup_sequence(void)
{
  clock_config();
  
	#ifdef __USING_TIMER
  /*** Taskclass ***/
  API_start_taskclass(&API_TIM3, T_PERIOD_S, 1);
#endif // __USING_TIMER
 
}

/*
* @brief config clock sources and prescalers
* @param[in] NONE
* @note  SYSCLK=48MHz
* @return NONE
*/
void clock_config(void)
{
  RCC_PLLCmd(DISABLE);
  
  RCC_HCLKConfig(RCC_SYSCLK_Div1);
  RCC_PCLK1Config(RCC_HCLK_Div2);
  RCC_PCLK2Config(RCC_HCLK_Div1);
  RCC_TIMCLKConfig(RCC_TIM1CLK_PCLK);
  
  RCC_PLLConfig(RCC_PLLSource_HSI_Div2, RCC_PLLMul_12);
  
  RCC_PLLCmd(ENABLE);
  while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET);
  
  RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
  
  SystemCoreClockUpdate();
}
